<?php
namespace Rschaufler\Localization\Localization\Parser;

class XliffParser extends \TYPO3\CMS\Core\Localization\Parser\XliffParser
{
    /**
     * @param \SimpleXMLElement $root
     * @return array
     */
    public function doParsingFromRoot(\SimpleXMLElement $root)
    {
        return parent::doParsingFromRoot($root);
    }
}
