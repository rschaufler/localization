<?php
namespace Rschaufler\Localization\Command;

use Rschaufler\Localization\Localization\Parser\XliffParser;
use TYPO3\CMS\Extbase\Mvc\Controller\CommandController;

class XliffCommandController extends CommandController
{
    /**
     * Converts an XLIFF file to CSV
     *
     * @param string $sourceFile Absolute path to XLIFF file to process
     * @param string $targetFile Absolute path to destination file
     * @return void
     */
    public function convertToCsvCommand($sourceFile, $targetFile)
    {
        if (is_readable($sourceFile)) {
            $xml = simplexml_load_file($sourceFile);

            if ($xml !== false) {
                $xliffParser = new XliffParser();

                $xliff = $xliffParser->doParsingFromRoot($xml);

                if (!file_exists($targetFile)) {
                    $handle = fopen($targetFile, 'w');

                    fclose($handle);
                }

                if (is_writable($targetFile)) {
                    $targetFileHandle = fopen($targetFile, 'w');

                    if ($targetFileHandle !== false) {
                        fputcsv($targetFileHandle, ['Key', 'Source', 'Target']);

                        foreach ($xliff as $key => $value) {
                            $input = [$key, '', ''];

                            if (isset($value[0])) {
                                $labels = $value[0];
                            }

                            if (isset($labels['source'])) {
                                $input[1] = $labels['source'];
                            }

                            if (isset($labels['target'])) {
                                $input[2] = $labels['target'];
                            }

                            fputcsv($targetFileHandle, $input);
                        }
                    }

                    fclose($targetFileHandle);
                }
            }
        }
    }
}
