<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Localization',
    'description' => 'Provides tools for localization',
    'category' => 'misc',
    'author' => 'Richard Schaufler',
    'author_email' => 'mail@richardschaufler.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.7.99'
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ]
    ]
];
