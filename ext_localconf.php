<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function($extKey)
    {
        if (TYPO3_MODE === 'BE') {
            $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][$extKey] =
                \Rschaufler\Localization\Command\XliffCommandController::class;
        }
    },
    $_EXTKEY
);
